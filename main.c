#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include "parser.h"
#include "run.h"
#include "signals.h"

Task *taskList;
bool reloadList = false, sigintReceived = false, printTasks = false;
char *outFile, *taskFile;
int fd;
int devNull;
int main( int argc, char *argv[] )
{
    if( argc < 3 ) {
        printf( "Usage:\n\t%s <taskfile> <outfile>\n", argv[ 0 ] );
        return EXIT_FAILURE;
    }

    // Daemon preparing
    pid_t pid, sid;
    pid = fork();
    if( pid < 0 )
        exit( EXIT_FAILURE );

    if( pid > 0 )
        exit( EXIT_SUCCESS );

    umask( 0 );

    sid = setsid();
    if( sid < 0 )
        exit( EXIT_FAILURE );

    close( STDIN_FILENO );
    close( STDOUT_FILENO );
    close( STDERR_FILENO );
    // Daemon is now working

    taskFile = argv[ 1 ];
    outFile = argv[ 2 ];
    fd = open( outFile, O_WRONLY | O_CREAT | O_APPEND, 0666 );
    devNull = open("/dev/null", O_WRONLY);
    /*
    dup2(fd, STDERR_FILENO);
    dup2(fd, STDOUT_FILENO);
    fprintf(stderr, "%s\n", strerror(errno));
    
    pid_t a = fork();
    
    if(a == 0) execlp("cat", "cat", "x.txt", 0);
    */
    taskList = parse( taskFile );
    //printTaskList( taskList );

    signal( SIGINT, handlerINT );
    signal( SIGUSR1, handlerUSR1 );
    signal( SIGUSR2, handlerUSR2 );

    while( true ) {
        runAll( taskList );

        while( !reloadList && !sigintReceived && !printTasks );

        if( sigintReceived )
            break;

        if( printTasks )
            printTasks = false;

        if( reloadList ) {
            freeTasksMemory( taskList );
            taskList = parse( taskFile );
            reloadList = false;
        }
    }

    freeTasksMemory( taskList );
    close( fd );

    return EXIT_SUCCESS;
}
