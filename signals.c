#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <signal.h>

#include "main.h"
#include "parser.h"

void handlerINT( int sig ) {
    if( sig == SIGINT ) {
        sigintReceived = true;
    }
}

void handlerUSR1( int sig ) {
    if( sig == SIGUSR1 ) {
        reloadList = true;
    }
}

void handlerUSR2( int sig ) {
    if( sig == SIGUSR2 ) {
        printTasks = true;
    }
}
