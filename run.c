#include <stdio.h> 
#include <stdlib.h> 
#include <sys/types.h> 
#include <unistd.h>
#include <time.h>
#include <stdbool.h>
#include <errno.h>
#include <syslog.h>
#include <string.h>
#include <sys/wait.h>

#include "parser.h"
#include "main.h"

void stream(int i, Task * task)
{
    int fds[2];
    pipe(fds);
    pid_t pid = fork();
    
    if(pid == 0)
    {
        close(fds[1]);
        dup2(fds[0], STDIN_FILENO);
        i++;
        if(task -> command[i+1] == NULL)
        {
            execvp(task -> command[i][0], task -> command[i]);
        }
        else
        {
            stream(i, task);
        }
    }
    else
    {
        close (fds[0]); 
        dup2 (fds[1], STDOUT_FILENO);
        execvp(task -> command[i][0], task -> command[i]);
    }
    return;
}

int getSleepTime( int H, int M ) {
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    int h = tm.tm_hour, m = tm.tm_min, s = tm.tm_sec;

    int time = (H - h) * 3600 + (M - m) * 60 - s;
    if(time < 0)
    {
        time += 86400;
    }
    return time;
}

char * writeProcessName(Task * pointer)
{
    int i = 0, j = 0, n = 10000;/*
    while(pointer -> command[i])
    {
        j = 0;
        while(pointer -> command[i][j])
        {
            n = n + sizeof(pointer -> command[i][j])/sizeof(*pointer -> command[i][j]);
            j++;
        }
        i++;
    }*/
    char * c = malloc(n);
    
    sprintf(c, "Process:  ");
    i = 0, j = 0;
    while(pointer -> command[i+1])
    {
        j = 0;
        while(pointer -> command[i][j])
        {
            char * d = malloc(n);
            char * e = malloc(n);
            sprintf(d, "%s ", pointer -> command[i][j]);
            sprintf(e, "%s%s", c, d);
            sprintf(c, "%s", e);
            free(d);
            free(e);
            j++;
        }
        i++;
        char * e = malloc(n);
        sprintf(e, "%s| ", c);
        sprintf(c, "%s", e);
        free(e);
    }
    
    j = 0;
    while(pointer -> command[i][j])
    {
        char * d = malloc(n);
        char * e = malloc(n);
        sprintf(d, "%s ", pointer -> command[i][j]);
        sprintf(e, "%s%s", c, d);
        sprintf(c, "%s", e);
        free(d);
        free(e);
        j++;
    }
    char * e = malloc(n);
    sprintf(e, "%s\n", c);
    sprintf(c, "%s", e);
    free(e);
    
    return c;
}

void runProcess(Task * pointer)
{
    if(pointer -> command[1] == NULL)
    {
        execvp(pointer -> command[0][0], pointer -> command[0]);
    }
    else
    {
        stream(0, pointer);
    }
}

void runAll(Task * task)
{
    Task * pointer = task;

    while(pointer -> prev != NULL) pointer = pointer -> prev;
    Task * start = pointer;
    
    while(1)
    {
        time_t t = time(NULL);
        struct tm tm = *localtime(&t);
        int h = tm.tm_hour, m = tm.tm_min, s = tm.tm_sec;
        int H = pointer -> hour, M = pointer -> minute;
        int time = (H - h) * 3600 + (M - m) * 60 - s;
        
        if(time > 0) break;
        
        pointer = pointer -> next;
        
        if(!pointer)
        {
            pointer = start;
            break;
        }
    }

    while(1)
    {
        do {
            if( printTasks ) {
                printTaskListToFile( pointer );
                printTasks = false;
            }

            if( reloadList || sigintReceived )
                return;

            int time = getSleepTime( pointer->hour, pointer->minute );
            sleep( time );
        } while( getSleepTime( pointer->hour, pointer->minute ) > 0 );

        if( reloadList || sigintReceived )
            return;

        if( printTasks ) {
            printTaskListToFile( pointer );
            printTasks = false;
        }
        
        
        pid_t pid = fork();

        if( pid == -1 ) {
            write( fd, "ERROR CREATING PROCESS", strlen( "ERROR CREATING PROCESS" ) );
        } if(pid == 0)
        {
            char * ch = writeProcessName(pointer);
            setlogmask(LOG_UPTO(LOG_NOTICE));
            openlog("minicron", LOG_CONS | LOG_PID |LOG_NDELAY, LOG_LOCAL1);
            syslog(LOG_NOTICE, "%s", ch);
            closelog();
            
            char * name = malloc(strlen(ch) + 10);
            sprintf(name, "\n%s\n", ch);
            write(fd, name, strlen(name));
            
            if(pointer -> info == 0)
            {
                dup2(devNull, STDERR_FILENO);
                dup2(fd, STDOUT_FILENO);
            }
            else
            if(pointer -> info == 1)
            {
                dup2(devNull, STDOUT_FILENO);
                dup2(fd, STDERR_FILENO);
                char * err = strerror(errno);
                char * error = malloc(strlen(err) + 20);
                sprintf(error, "Process error: %s\n", err);
                write(fd, error,strlen(error));
            }
            else
            if(pointer -> info == 2)
            {
                dup2(fd, STDOUT_FILENO);
                dup2(fd, STDERR_FILENO);
                char * err = strerror(errno);
                char * error = malloc(strlen(err) + 20);
                sprintf(error, "Process error: %s\n", err);
                write(fd, error,strlen(error));
            }
            
            runProcess(pointer);
        } else {
            int status = 0;
            if( waitpid( pid, &status, 0) != -1 ) {
                if( WIFEXITED( status ) ) {
                    openlog( "minicron", LOG_CONS | LOG_PID |LOG_NDELAY, LOG_LOCAL1 );
                    syslog( LOG_NOTICE, "Exit code: %d", WEXITSTATUS(status) );
                    closelog();
                }
            }
        }

        pointer = pointer -> next;
        if( pointer == NULL )
            return;
    }
    
}