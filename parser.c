#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

#include "parser.h"
#include "main.h"

Task* parse( char taskfile[] ) {

    unsigned char buffer[ 64 ];
    size_t bytes_read;
    int i;

    LineStatus lineStatus = BEGIN;
    Task *tmpTask = NULL;
    Task *list = NULL;
    int reject = 0;
    bool pipe = false;
    int line = 1;

    bool apostrophe1 = false;
    bool apostrophe2 = false;

    TempCommandsArray *tempCommandsArray = NULL;
    TempStr *tempStr = NULL;
    int fd = open( taskfile, O_RDONLY );
    do {
        bytes_read = read( fd, buffer, sizeof( buffer ) );

        for( i = 0; i < bytes_read; i++ ) {
            if( buffer[ i ] == '\n' && lineStatus != NEWLINEWAIT ) {
                reject = 3;
                lineStatus = NEWLINEWAIT;
            }

            if( lineStatus == BEGIN ) {
                tmpTask = malloc( sizeof( Task ) );
                tmpTask->prev = NULL;
                tmpTask->next = NULL;
                tmpTask->command = NULL;
                lineStatus = HOUR;
                tmpTask->hour = 0;
                tmpTask->minute = 0;
                tmpTask->info = 0;
            }

            if( lineStatus == HOUR ) {
                if( buffer[ i ] == ':' ) {
                    if( 23 < tmpTask->hour || tmpTask->hour < 0 ) {
                        reject = 1;
                        lineStatus = NEWLINEWAIT;
                        continue;
                    }
                    lineStatus = MINUTE;
                    continue;
                }
                tmpTask->hour *= 10;
                tmpTask->hour += ( buffer[ i ] - '0' );
            }

            if( lineStatus == MINUTE ) {
                if( buffer[ i ] == ':' ) {
                    if( 59 < tmpTask->minute || tmpTask->minute < 0 ) {
                        reject = 2;
                        lineStatus = NEWLINEWAIT;
                        continue;
                    }

                    tempCommandsArray = malloc( sizeof( TempCommandsArray ) );
                    tempCommandsArray->array = malloc( sizeof( char ** ) );
                    tempCommandsArray->curPos = 0;
                    tempCommandsArray->curSize = 0;

                    tempStr = malloc( sizeof( TempStr ) );
                    tempStr->str = calloc( ARRAY_INITIAL_SIZE, sizeof( char ) );
                    tempStr->curPos = 0;
                    tempStr->curLength = ARRAY_INITIAL_SIZE;

                    lineStatus = COMMAND;
                    continue;
                }
                tmpTask->minute *= 10;
                tmpTask->minute += ( buffer[ i ] - '0' );
            }

            if( lineStatus == COMMAND ) {
                if( pipe && buffer[ i ] == ' ' )
                    continue;
                if( pipe )
                    pipe = false;

                if( ( buffer[ i ] == '|' ||  buffer[ i ] == ':' ) && ( !apostrophe1 && !apostrophe2 ) ) {
                    if( tempStr->str[ tempStr->curPos - 1 ] == ' ' ) {
                        tempStr->curPos--;
                        tempStr->str[ tempStr->curPos ] = 0;
                    }

                    char ***tmpCommandsArray = realloc( tempCommandsArray->array, sizeof( char ** ) * ( tempCommandsArray->curSize + 1 ) );
                    if( tmpCommandsArray == NULL ) {
                        char buf[ 100 ];
                        sprintf( buf, "\nERRORS:\nP108 Error alocating memory.\n" );
                        write( fd, buf, strlen( buf ) );
                        close( fd );
                        exit( EXIT_FAILURE );
                    }
                    tempCommandsArray->array = tmpCommandsArray;
                    tempCommandsArray->curSize++;
                    tempCommandsArray->array[ tempCommandsArray->curPos ] = parseCommandStr( tempStr->str );
                    tempCommandsArray->curPos++;

                    if( buffer[ i ] == '|' ) {
                        if( tempStr->str != NULL )
                            free( tempStr->str );
                        if( tempStr != NULL )
                            free( tempStr );

                        tempStr = malloc( sizeof( TempStr ) );
                        tempStr->str = calloc( ARRAY_INITIAL_SIZE, sizeof( char ) );
                        tempStr->curPos = 0;
                        tempStr->curLength = ARRAY_INITIAL_SIZE;
                        pipe = true;
                        continue;
                    }

                    if( buffer[ i ] == ':' ) {
                        tmpCommandsArray = realloc( tempCommandsArray->array, sizeof( char ** ) * ( tempCommandsArray->curSize + 1 ) );
                        if( tmpCommandsArray == NULL ) {
                            char buf[ 100 ];
                            sprintf( buf, "\nERRORS:\nP136 Error alocating memory.\n" );
                            write( fd, buf, strlen( buf ) );
                            close( fd );
                            exit( EXIT_FAILURE );
                        }
                        tempCommandsArray->array = tmpCommandsArray;
                        tempCommandsArray->array[ tempCommandsArray->curPos ] = NULL;
                        tmpTask->command = tempCommandsArray->array;
                        lineStatus = INFO;
                        if( tempCommandsArray != NULL )
                            free( tempCommandsArray );
                        if( tempStr->str != NULL )
                            free( tempStr->str );
                        if( tempStr != NULL )
                            free( tempStr );

                        continue;
                    }
                }
                if( tempStr->curPos + 2 >= tempStr->curLength ) {
                    char *tmpStr = realloc( tempStr->str, sizeof( char ) * tempStr->curLength * 2 );
                    if( tmpStr == NULL ) {
                        char buf[ 100 ];
                        sprintf( buf, "\nERRORS:\nP159 Error alocating memory.\n" );
                        write( fd, buf, strlen( buf ) );
                        close( fd );
                        exit( EXIT_FAILURE );
                    }
                    tempStr->str = tmpStr;
                    tempStr->curLength *= 2;
                }
                tempStr->str[ tempStr->curPos ] = buffer[ i ];
                tempStr->str[ tempStr->curPos + 1 ] = 0;
                tempStr->curPos++;

                if( buffer[ i ] == '"' && !apostrophe1 && !apostrophe2 ) {
                    apostrophe1 = true;
                    continue;
                } else if( buffer[ i ] == '"' && apostrophe1 ) {
                    apostrophe1 = false;
                    continue;
                }

                if( buffer[ i ] == 39 && !apostrophe1 && !apostrophe2 ) {
                    apostrophe2 = true;
                    continue;
                } else if( buffer[ i ] == 39 && apostrophe2 ) {
                    apostrophe2 = false;
                    continue;
                }
            }

            if( lineStatus == INFO ) {
                tmpTask->info = buffer[ i ] - '0';
                lineStatus = NEWLINEWAIT;
                if( !reject || 2 >= tmpTask->info || tmpTask->info >= 0 )
                    addToTaskList( &list, tmpTask );
                continue;
            }

            if( lineStatus == NEWLINEWAIT && buffer[ i ] == '\n' ) {
                if( reject ) {
                    if( reject != 3 )
                        freeTaskMemory( tmpTask );

                    char buf[ 100 ];

                    sprintf( buf, "\nERRORS:\nError parsing line %d, parsing group: %d\n", line, reject );
                    write( fd, buf, strlen( buf ) );

                    reject = 0;
                }
                Task *tmpTask = NULL;
                Task *list = NULL;
                line++;
                lineStatus = BEGIN;
                continue;
            }
        }

    } while( bytes_read == sizeof( buffer ) );

    close( fd );

    adjustList( &list );

    return list;
}

// Pointer on pointer to modify first item on list
void addToTaskList( Task **first, Task *newTask ) {
    if( *first == NULL ) {
        ( *first ) = newTask;
        ( *first )->prev = NULL;
        ( *first )->next = NULL;
        return;
    }

    if( (*first)->hour > newTask->hour || ( (*first)->hour == newTask->hour && (*first)->minute > newTask->minute ) ) {
        Task *old = *first;
        old->prev = newTask;
        ( *first ) = newTask;
        ( *first )->prev = NULL;
        ( *first )->next = old;
        return;
    }

    Task *pointer;
    pointer = *first;
    int nTaskHour = newTask->hour;
    int nTaskMinute = newTask->minute;

    while( pointer->next != NULL ) {
        // For simplicity
        int thisHour = pointer->hour;
        int thisMinute = pointer->minute;
        int nextHour = pointer->next->hour;
        int nextMinute = pointer->next->minute;

        if( thisHour <= nTaskHour && nTaskHour <= nextHour && thisMinute <= nTaskMinute && nTaskMinute <= nextMinute )
            break;

        pointer = pointer->next;
    }
    if( pointer->next != NULL ) {
        newTask->next = pointer->next;
        pointer->next = newTask;
    } else
        newTask->next = NULL;
    newTask->prev = pointer;
    pointer->next = newTask;
}

void adjustList( Task **first ) {
    if( *first == NULL )
        return;

    int cHour, cMinute, tHour, tMinute;
    time_t rawtime;
    struct tm *timeinfo;

    time( &rawtime );
    timeinfo = localtime( &rawtime );

    cHour = timeinfo->tm_hour;
    cMinute = timeinfo->tm_min;

    Task *pointer = *first;

    while( true ) {
        tHour = pointer->hour;
        tMinute = pointer->minute;

        if( tHour > cHour || ( tHour == cHour && tMinute > cMinute ) )
            break;

        pointer = pointer->next;
        if( pointer == NULL )
            return;
    }

    if( pointer->prev == NULL )
        return;

    Task *nFirst = pointer;
    Task *nLast = pointer->prev;

    while( pointer->next != NULL )
        pointer = pointer->next;

    Task *nMidLeft = pointer;
    Task *nMidRight = *first;

    nFirst->prev = NULL;
    (*first) = nFirst;
    nLast->next = NULL;
    nMidLeft->next = nMidRight;
    nMidRight->prev = nMidLeft;
}

char **parseCommandStr( char *str ) {
    int i;

    TempStr *tempStr = malloc( sizeof( TempStr ) );
    tempStr->str = calloc( ARRAY_INITIAL_SIZE, sizeof( char ) );
    tempStr->curPos = 0;
    tempStr->curLength = ARRAY_INITIAL_SIZE;

    TempStrArray *tempStrArray = malloc( sizeof( TempStrArray ) );
    tempStrArray->array = malloc( sizeof( char * ) );
    tempStrArray->curPos = 0;
    tempStrArray->curSize = 0;
    tempStrArray->array = NULL;


    for( i = 0;; i++ ) {

        if( str[ i ] == ' ' || str[ i ] == 0 ) {
            // Next parameter, free unused memory, and allocate memory for ended parameter
            char *tmpStr = realloc( tempStr->str, sizeof( char ) * ( tempStr->curPos + 1 ) );
            char **tmpStrArray = realloc( tempStrArray->array, sizeof( char * ) * ( tempStrArray->curSize + 1 ) );

            // If allocating error kill process
            if( tmpStrArray == NULL || tmpStr == NULL ) {
                char buf[ 100 ];
                sprintf( buf, "\nERRORS:\nP341 Error alocating memory.\n" );
                write( fd, buf, strlen( buf ) );
                close( fd );
                exit( EXIT_FAILURE );
            }
            // String end with 0
            tempStr->str = tmpStr;
            tempStr->str[ tempStr->curPos ] = 0;

            // Parameters array = temporary array, current size bigger
            tempStrArray->array = tmpStrArray;
            tempStrArray->array[ tempStrArray->curPos ] = tempStr->str;
            tempStrArray->curSize++;
            tempStrArray->curPos++;

            if( str[ i ] != 0 ) {
                if( tempStr != NULL )
                    free( tempStr );

                tempStr = calloc( 1, sizeof( TempStr ) );
                tempStr->str = calloc( ARRAY_INITIAL_SIZE, sizeof( char ) );
                tempStr->curPos = 0;
                tempStr->curLength = ARRAY_INITIAL_SIZE;
                continue;
            }

            // Ending of command parsing
            tmpStrArray = realloc( tempStrArray->array, sizeof( char * ) * ( tempStrArray->curSize + 1 ) );
            if( tmpStrArray == NULL ) {
                char buf[ 100 ];
                sprintf( buf, "\nERRORS:\nP371 Error alocating memory.\n" );
                write( fd, buf, strlen( buf ) );
                close( fd );
                exit( EXIT_FAILURE );
            }
            tempStrArray->array = tmpStrArray;
            tempStrArray->curSize++;
            tempStrArray->array[ tempStrArray->curPos ] = NULL;
            break;
        }
        if( tempStr->curPos + 2 >= tempStr->curLength ) {
            char *tmpStr = realloc( tempStr->str, sizeof( char ) * tempStr->curLength * 2 );
            if( tmpStr == NULL ) {
                char buf[ 100 ];
                sprintf( buf, "\nERRORS:\nP385 Error alocating memory.\n" );
                write( fd, buf, strlen( buf ) );
                close( fd );
                exit( EXIT_FAILURE );
            }
            tempStr->str = tmpStr;
            tempStr->curLength *= 2;
        }
        tempStr->str[ tempStr->curPos ] = str[ i ];
        tempStr->str[ tempStr->curPos + 1 ] = 0;
        tempStr->curPos++;
    }

    char **array = tempStrArray->array;

    if( tempStr != NULL )
        free( tempStr );
    if( tempStrArray != NULL )
        free( tempStrArray );

    return array;
}

void freeTaskMemory( Task *task ) {
    char ***command = task->command;
    int i = 0;
    while( command[ i ] != NULL ) {
        int i2 = 0;
        while( command[ i ][ i2 ] != NULL ) {
            free( command[ i ][ i2 ] );
            i2++;
        }
        free( command[ i ] );
        i++;
    }
    free( command );
    free( task );
}

void freeTasksMemory( Task *list ) {
    Task *pointer = list;
    Task *old = NULL;
    while( pointer->prev != NULL ) {
        pointer = pointer->prev;
    }

    while( pointer != NULL ) {
        int i = 0;
        while( pointer->command[ i ] != NULL ) {
            int i2 = 0;
            while( pointer->command[ i ][ i2 ] != NULL ) {
                free( pointer->command[ i ][ i2 ] );
                i2++;
            }
            free( pointer->command[ i ] );
            i++;
        }
        free( pointer->command );
        old = pointer;
        pointer = pointer->next;
        free( old );
    }
}

void printTaskListToFile( Task *list ) {
    Task *pointer = list;
    char buf[ 500 ];

    sprintf( buf, "\nREMAINING TASKS:\n" );
    write( fd, buf, strlen( buf ) );

    while( pointer != NULL ) {
        sprintf( buf, "Godzina: %d, Minuta: %d, Komendy: ( ", pointer->hour, pointer->minute );
        write( fd, buf, strlen( buf ) );

        fflush( stdout );
        int i = 0;
        while( pointer->command[ i ] != NULL ) {
            int i2 = 0;
            sprintf( buf, "Komenda %d: { ", i );
            write( fd, buf, strlen( buf ) );
            while( pointer->command[ i ][ i2 ] != NULL ) {
                sprintf( buf, "%s ", pointer->command[ i ][ i2 ] );
                write( fd, buf, strlen( buf ) );
                fflush( stdout );
                i2++;
            }
            sprintf( buf, "} " );
            write( fd, buf, strlen( buf ) );
            i++;
        }
        sprintf( buf, ") info: %d\n", pointer->info );
        write( fd, buf, strlen( buf ) );

        pointer = pointer->next;
    }
}

//DO NOT USE IN DAEMON MODE!!!!!
void printTaskList( Task *list ) {
    Task *pointer = list;
    while( pointer->prev != NULL ) {
        pointer = pointer->prev;
    }

    while( pointer != NULL ) {
        printf( "Godzina: %d, Minuta: %d, Komendy: ", pointer->hour, pointer->minute );
        fflush( stdout );
        int i = 0;
        while( pointer->command[ i ] != NULL ) {
            int i2 = 0;
            printf( "com %d: ", i );
            fflush( stdout );
            while( pointer->command[ i ][ i2 ] != NULL ) {
                printf( "%s ", pointer->command[ i ][ i2 ] );
                fflush( stdout );
                i2++;
            }
            printf( ", " );
            fflush( stdout );
            i++;
        }
        printf( "info: %d\n", pointer->info );
        fflush( stdout );
        pointer = pointer->next;
    }
}
