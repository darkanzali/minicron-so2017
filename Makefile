OBJ = main.o parser.o run.o signals.o

CC = gcc

all: minicron

minicron: $(OBJ)
	gcc $(OBJ) -o minicron

debug:
	gcc $(OBJ) -g -o minicron

clean:
	rm *.o minicron
