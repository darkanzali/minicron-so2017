#ifndef _main_h_
#define _main_h_

#include "parser.h"

extern Task *taskList;
extern bool reloadList;
extern bool sigintReceived;
extern bool printTasks;
extern char* outFile;
extern int fd;
extern int devNull;

#endif
