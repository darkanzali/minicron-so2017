#ifndef __parser_h__
#define __parser_h__

#define ARRAY_INITIAL_SIZE  5

/*
 * Command array in task:
 *
 * command[i]
 *   |
 *   |
 *   |-->command[0][i]--| // Element with array of parameters for first command
 *   |                  |
 *   |                  |-->command[0][0][i]--| // Parameters to execute, char array (string)
 *   |                  |                     |--> char
 *   |                  |                     |--> last char = 0
 *   |                  |
 *   |                  |-->command[0][0][LAST] = NULL // Last element in array is NULL
 *   |
 *   |-->command[LAST] = NULL // Last element in array is NULL
 *
 *
 *   Example ( ls -l -a | wc -l )
 *
 *   command (char ***)
 *    |
 *    |   --------
 *    |-->| ls   |  <- Simplified rows, actual looks like:
 *    |   | -l   |        ----------
 *    |   | -a   |        | l      |
 *    |   | NULL |        | s      |
 *    |   --------        | (int)0 | <- in ascii table known as NUL
 *    |                   ----------
 *    |   --------
 *    |-->| wc   |
 *    |   | -l   |
 *    |   | NULL |
 *    |   --------
 *    |
 *    |-->NULL
 */

typedef struct task {
    int hour;
    int minute;
    int info;
    struct task *prev;
    struct task *next;
    char ***command;
} Task;

typedef struct {
    int curSize;
    int curPos;
    char ***array;
} TempCommandsArray;

// Temporary string array with info for realloc and insert data
typedef struct {
    int curSize;
    int curPos;
    char **array;
} TempStrArray;

// Temporary string with info for realloc and insert data
typedef struct {
    int curLength;
    int curPos;
    char *str;
} TempStr;

typedef enum {  BEGIN, // Parsing line just started, allocate memory for task
                HOUR,
                MINUTE,
                COMMAND,
                INFO,
                NEWLINEWAIT // Ending line parsing, continue till newline
    } LineStatus;

Task* parse( char taskfile[] );
void addToTaskList( Task **list, Task *newTask );
void adjustList( Task **first );
char **parseCommandStr( char *str );
void freeTaskMemory( Task *task );
void freeTasksMemory( Task *list );
void printTaskListToFile( Task *list );

//DEBUG FUNCTIONS
void printTaskList( Task *list );

#endif

